@extends('layouts.master')


@section('body')

<div class="container">
   
    <div class="row" style="margin-top:3em;">
        <div class="col-md-6 col-md-offset-2">
            <div class="post">
            	<h3> Post title </h3> 
            	<img src="Anonymous.jpg" width="555" height="450">
				<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam quas, facilis, incidunt inventore cupiditate omnis non. Totam ut aliquid, iste similique, porro sequi hic modi iusto eius molestias. Ratione, quos.
        		</p>
        		<a class="btn btn-primary" href"#">Read more </a>
			</div>

			<div class="post">
            	<h3> Post title </h3> 
            	<img src="Anonymous.jpg" width="555" height="450">
				<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam quas, facilis, incidunt inventore cupiditate omnis non. Totam ut aliquid, iste similique, porro sequi hic modi iusto eius molestias. Ratione, quos.
        		</p>
        		<a class="btn btn-primary" href"#">Read more </a>
			</div>

			<div class="post">
            	<h3> Post title </h3> 
            	<img src="Anonymous.jpg" width="555" height="450">
				<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam quas, facilis, incidunt inventore cupiditate omnis non. Totam ut aliquid, iste similique, porro sequi hic modi iusto eius molestias. Ratione, quos.
        		</p>
        		<a class="btn btn-primary" href"#">Read more </a>
			</div>


        </div>

        <div class="col-md-3" style="margin-left:3em; margin-top:1em;">
            <div class="post">


				<h3 style="text-align:center; margin-bottom:1em;"> Profile name </h3>
				<img src="profilepicture.jpg" width="250" height="250" style="border-radius:1%; margin-bottom:2em;">
        		<p style="margin-bottom:2em;"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus magni error cumque eos nam vitae quam pariatur atque, aliquid vel non animi ullam nulla. Nulla debitis, adipisci temporibus dicta similique!
        		</p>
        		<a class="btn btn-primary" href"#">Follow</a>
        		<a class="btn btn-primary" href"#">Edit Profile</a>
        		<a class="btn btn-primary" href"#">Log Out</a>
        		
			</div>
        </div>
    </div>


</div>

@stop
